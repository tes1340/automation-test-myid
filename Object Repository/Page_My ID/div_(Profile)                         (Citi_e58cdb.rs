<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_(Profile)                         (Citi_e58cdb</name>
   <tag></tag>
   <elementGuidId>96027749-6191-4f4f-90cc-1efd4a8701ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-lg-12</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-12</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                	
ข้อมูลส่วนบุคคล (Profile)


    
        
            หมายเลขบัตรประชาชน (Citizen ID)1200101883877
            รหัสนิสิต (Student code)
          62160006
        
        
          ชื่อ - นามสกุล (Name)
          NAPHAT TISONTHI
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-07 13:04:49 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-02-12 02:01:20
        
    

 

 


                    
                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                	
ข้อมูลส่วนบุคคล (Profile)


    
        
            หมายเลขบัตรประชาชน (Citizen ID)1200101883877
            รหัสนิสิต (Student code)
          62160006
        
        
          ชื่อ - นามสกุล (Name)
          NAPHAT TISONTHI
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-07 13:04:49 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-02-12 02:01:20
        
    

 

 


                    
                    
                ' or . = '
                	
ข้อมูลส่วนบุคคล (Profile)


    
        
            หมายเลขบัตรประชาชน (Citizen ID)1200101883877
            รหัสนิสิต (Student code)
          62160006
        
        
          ชื่อ - นามสกุล (Name)
          NAPHAT TISONTHI
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-07 13:04:49 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-02-12 02:01:20
        
    

 

 


                    
                    
                ')]</value>
   </webElementXpaths>
</WebElementEntity>
