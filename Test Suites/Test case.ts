<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test case</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>e369b619-5aa6-44d5-aaa2-0caa67cd09fc</testSuiteGuid>
   <testCaseLink>
      <guid>6aba9e81-2c8c-47f8-97ab-e7a12fa44756</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01 Change Password Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a6adbb1-0421-4761-abbe-4b5b83f2edaf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Not have number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cd45ad7-0e74-4137-83eb-1c2a116016f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change not have symbol</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8769da0-1a6b-4e9f-835e-9054d181a22b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>996556ae-b929-4ce5-8f86-3f5dba486247</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case dont have character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dd3acd2-107c-4096-8a66-82eea1bf2d93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login Fail id wrong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01b39927-f60a-4abb-8a6d-c9bfcfbac238</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login Fail password wrong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1333fc5a-9d70-451b-9055-a834b6976d27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Logout success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e48de320-bd50-4a12-a2e6-2d862dc29607</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Texst case number lesster 8</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
