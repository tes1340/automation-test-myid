<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tr_(Citizen ID)120010188             (Stude_9d3d1b</name>
   <tag></tag>
   <elementGuidId>74731dc9-5b38-4c38-ae67-71f1113ed8ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div/table/tbody/tr</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>tr</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>tr</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    หมายเลขบัตรประชาชน (Citizen ID)120010188****
            รหัสนิสิต (Student code)
          62160006
        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/table/tbody/tr</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ข้อมูลส่วนบุคคล (Profile)'])[1]/following::tr[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//tr[(text() = '
                    หมายเลขบัตรประชาชน (Citizen ID)120010188****
            รหัสนิสิต (Student code)
          62160006
        ' or . = '
                    หมายเลขบัตรประชาชน (Citizen ID)120010188****
            รหัสนิสิต (Student code)
          62160006
        ')]</value>
   </webElementXpaths>
</WebElementEntity>
