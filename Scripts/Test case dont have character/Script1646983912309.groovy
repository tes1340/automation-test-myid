import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.junit.Assert as Assert
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://myid.buu.ac.th/changepwd/chkuser')

WebUI.setText(findTestObject('Object Repository/Page_My ID/input_(Username)_user'), '62160006')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Enter'))

WebUI.click(findTestObject('Object Repository/Page_My ID/input'))

WebUI.setText(findTestObject('Object Repository/Page_My ID/input_(Old Password)_oldpass'), '@axdza789')

WebUI.setText(findTestObject('Object Repository/Page_My ID/input_(New Password)_newpass'), '@12345678')

WebUI.setText(findTestObject('Object Repository/Page_My ID/input_(Re-New Password)_renewpass'), '@12345678')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Change Password'))

actual = WebUI.getText(findTestObject('Object Repository/Page_My ID/div_8   25 Password more than 8 - 25 character'))
expected = 'ไม่พบตัวอักษรภาษาอังกฤษ ในรหัสผ่านใหม่ของท่าน\nNot found characters in your new password'
Assert.assertEquals(expected, actual)

WebUI.closeBrowser()

